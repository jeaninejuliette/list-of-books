import os, re, time
import pandas as pd
from isbntools.app import *
import urllib.request
import json

# the link where to retrieve the book summary
base_api_link = "https://www.googleapis.com/books/v1/volumes?q=isbn:"

# the directory where the books can be found and the current list of books (if exists)
bookdir = "Z:/Jeanine/Boeken" #os.getcwd()
current_books = os.path.join(bookdir, 'boekenlijst.xlsx')

# check to see if there is a list with books available already
# if this file does not exist already, this script will create it automatically
if os.path.exists(current_books):
    my_current_books = pd.read_excel(current_books, dtype='object')
else:
    my_current_books = pd.DataFrame(columns=["index", "ISBN", "summary", "location"])

# create an empty dictionary
my_books = {}

print("-------------------------------------------------")
print(time.strftime("%d-%m-%Y %H:%M:%S", time.localtime()))
print("-------------------------------------------------")
print("Starting to list all books (epub and pdf) in the given directory")

# create a list of all books (epub or pdf files) in the directory and all its subdirectories
# r=root, d=directories, f=files
for r, d, f in os.walk(bookdir):
    for file in f:
        if (file.endswith(".epub")) or (file.endswith(".pdf")):
            # Remove the text _Jeanine_ 1234567891234 from the filenames
            booktitle = re.sub('.epub', '', re.sub('_Jeanine _\d{13}', '', file))
            booklocation = re.sub(bookdir, '', r)
            my_books[booktitle] = booklocation

print("-------------------------------------------------")
print(f"Found {len(my_books.keys())} books in the given directory")
print(f"Found {len(my_current_books)} in the existing list of books")

# only keep the books that were not already in the list
if len(my_current_books) > 0:
    for d in my_current_books["index"]:
        try:
            del my_books[d]
        except KeyError:
            pass # if a key is not found, this is no problem

print("-------------------------------------------------")
print(f"There are {len(my_books.keys())} books that were not already in the list of books")
print("-------------------------------------------------\n")

# try to get more information on each book
i = 0
for book, location in my_books.items():
    print(f"Processing: {book}, this is number {i+1} in the list")
    isbn = 0
    summary = ""
    try:
        # retrieve ISBN
        isbn = isbn_from_words(book)
        # retrieve book information from Google Books
        if len(isbn) > 0:
            with urllib.request.urlopen(base_api_link + isbn) as f:
                text = f.read()
            decoded_text = text.decode("utf-8")
            obj = json.loads(decoded_text) 
            volume_info = obj["items"][0]
            summary = re.sub(r"\s+", " ", volume_info["searchInfo"]["textSnippet"])
    except Exception as e:
        print(f"got an error when looking for {book}, the error is: {e}")
    my_books[book] = {"location":location, "ISBN":isbn, "summary":summary}
    i += 1
    # sleep to prevent 429 time-out error in the API request to get the ISBN
    time.sleep(5)

# write to Excel
all_books = pd.DataFrame(data=my_books)
all_books = (all_books.T)
all_books = all_books.reset_index()
all_books = pd.concat([my_current_books, all_books])
all_books.to_excel(current_books, index=False)

print("-------------------------------------------------")
print(time.strftime("%d-%m-%Y %H:%M:%S", time.localtime()))
print("-------------------------------------------------")