# Create a list of all my e-books, including their summary according to Google Books

Create an overview of all e-books (epub and pdf format) on a specific directory (and all of its sub-directories) on your computer. Also try to find the ISBN of each book and the Google Summary belonging to that ISBN.
If, like me, you are a book lover who devours books, you are probably also familiar with the problem of bringing enough books while traveling. This is where the e-book entered my life, and it is definitely here to stay. But how to keep track of all my e-books? And how do I remember what they’re about by just seeing their title? And, the worst of all problems, how to pick the next book to read from the huge virtual pile of books?
<br>
<br>
After struggling with this for years, I finally decided it was time to spend some time to solve this problem. Why not create a Python script to list all my books, retrieve their ISBN and finally find their summary on Google Books, ending up with an Excel file containing all this information? And yes, this is as simple as it sounds! I’ve used the brilliantly simple isbntools package and combined this with the urllib package to get the summary from Google Books. Easy does it! 
<br>
<br>
Have fun creating your own list of all your e-books!